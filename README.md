# Groupbuilder

A fluent lua library for creating DCS (DigitalCombatSimulator) groups quickly in code.


## Installation

I currently use groupbuilder as a git submodule in the projects I want to use it in. You can also add it to `DCS.openbeta/Scripts/groupbuilder` and have it provided for all missions.

Groupbuilder is provided as lua modules. So you can include it in your mission scripting with the following:

```lua
local groupbuilder = require("groupbuilder.groupbuilder")
local unitbuilder = require("groupbuilder.unitbuilder")
local groupimporter = require("groupbuilder.groupimporter")
```


## Usage

Groupbuilder currently provides three objects of interest:

  - `GroupBuilder` builds groups
  - `UnitBuilder` builds units
  - `GroupImporter` imports units from a .stm file.

You can see it in use at PGAW's [SCUDs system](https://gitlab.com/hoggit/developers/persian-gulf-at-war/-/blob/master/scuds.lua).

### GroupBuilder

The group builder is a [fluent interface](https://en.wikipedia.org/wiki/Fluent_interface) for creating groups for DCS. Fluent interfaces use method chaining to build up to the desired state.

To create a brand new empty group called "MyGroup": 
```lua
local group = GroupBuilder.New("MyGroup")
```

To create a brand new empty group called "MyGroup" that will spawn at x=10,y=5:

```lua
local group = GroupBuilder.New("MyGroup"):atPosition({x:10,y:5})
```

To create a new group called "MyGroup" and add units created with UnitBuilder:

```lua

local units = myUnits -- See unitbuilder docs for how to build units.
local group = GroupBuilder.New("MyGroup"):withUnits(units)
```

You can chain these methods to build up groups:

```lua
local group = GroupBuilder.New("MyGroup"):withCountry("USA"):atPosition({x:10,y:5}):withUnits(units)
```

### UnitBuilder

UnitBuilder's usage is very similar to GroupBuilder, except that instead of creating the groups, you are creating the units.

To create UnitBuilder for the `BMP-2`:

```lua
local unit = UnitBuilder.New("BMP-2")
```

Methods can also be chained in UnitBuilder

```lua
local unit = UnitBuilder.New("BMP-1"):withSkill("Excellent"):withHeading(180):withName("Target")
```

UnitBuilder provides some globals for getting units, see `UnitBuilder.Ground` namespace for it.

### GroupLoader

GroupLoader is used for loading DCS template files (`.stm`) for use in GroupBuilder. You can create a mission template with groups and units, then save it using `Edit -> Save Static Template` in the ME. A new file will be created at `DCS.openbeta/StaticTemplates/` with the name of your template.

You can load that template into the GroupBuilder system with:
```lua
local groups = GroupLoader.LoadFile(lfs.writedir() .. "StaticTemplate/New template.stm")
```

GroupLoader returns a map of the groups from the template, since there can be more than one. The map is organized by group _name_ as provided in the ME. So if you stored a group "Group1" in the template you just loaded, you would get the `Group1` GroupBuilder by doing the following:

```lua
local groups = GroupLoader.LoadFile(lfs.writedir() .. "StaticTemplate/New template.stm")
local group1 = groups["Group1"]
```


### Spawning

Once your group is ready to be spawned, you can call the `:spawn()` method on the GroupBuilder.
This method, by default, requires [mist](https://github.com/mrSkortch/MissionScriptingTools) because the default spawn function is `mist.dynAdd`. 

If you don't have mist installed, then you can replace the global `GroupBuilder.SpawnFunction` to the spawn function you want to use. The new SpawnFunction must receive a single argument that is the group to spawn.  

**Notes about spawning**

When we call spawn on a group we do a few actions prior to spawning it.
1. We attempt to make sure that the name is unique from previous uses of the same groupbuilder. 
2. We update the unit positions so that they are centered around the groups position. Both units and groups can have positions, so if the group position has been set then we will override the unit's position with the groups position.

## Chaining


GroupBuilder copies itself whenever you chain methods, so the new GroupBuilder you get back is a seperate one from the previous one. The old one still exists, and any handles to it will still be valid, and if it's a valid group it can still be spawned in its old configuration! As an Example:

```lua
local group1 = groups["group1"]
local group2 = group1:atPosition({x:group1.x+5, x:group1.y+10})
group1:spawn()
group2:spawn() --- Spawns the same composition, just a few units away from the previous group.
```
