--- UnitBuilder module
UnitBuilder = {}

local function copy(obj, seen)
  if type(obj) ~= 'table' then return obj end
  if seen and seen[obj] then return seen[obj] end
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do res[copy(k, s)] = copy(v, s) end
  return res
end

--- Creates a new UnitBuilder for a unit of {{unitType}}
-- @param unitType The DCS unit designation.
-- @return a new UnitBuilder with a type of {{unitType}}
UnitBuilder.New = function(unitType)
    return {
        ["type"] = unitType,
        playerCanDrive = false,
        skill = "Average",
        heading = 0,
        --- Sets this unit's positional offset from the center of the group
        -- If offset is set, then we will arrange this unit's position
        -- to be {+x,+y} away from the center of the group.
        -- @param self This UnitBuilder
        -- @param offset The {x,y} to apply to the group's position.
        -- @return a new UnitBuilder with the new position.
        withOffset = function(self, offset)
            local other = copy(self)
            other.offset = offset
            return other
        end,

        --- Sets a unit's position to a global location.
        -- This is ignored if the unit's offset is set.
        -- @param self This UnitBuilder
        -- @param position The {x,y} coordinates to spawn this unit.
        -- @return a new UnitBuilder with the new position
        withPosition = function(self, position)
            local other = copy(self)
            other.position = position
            other.x = position.x
            other.y = position.y
            return other
        end,


        --- Creates a copy of the UnitBuilder where the type is {unit_type}
        -- @param self This UnitBuilder
        -- @param unit_type The DCS designation for this unit.
        -- @return a new UnitBuilder with the new type
        withType = function(self, unit_type)
            local other = copy(self)
            other.type = unit_type
            return other
        end,

        --- Creates a copy of the UnitBuilder where the skill is {skill}
        -- @param self This UnitBuilder
        -- @param skill The new skill level. e.g. "Excellent"
        -- @return a new UnitBuilder with the new skill
        withSkill = function(self, skill)
            local other = copy(self)
            other.skill = skill
            return other
        end,


        --- Creates a copy of the UnitBuilder where the new heading is {heading}
        -- @param self This UnitBuilder
        -- @param heading The new heading
        withHeading = function(self, heading)
            local other = copy(self)
            other.heading = heading
            return other
        end,

        --- Creates a copy of the UnitBuilder where the new group name is {name}
        -- @param self This UnitBuilder
        -- @param name The new unit name
        -- @return a new UnitBuilder with the new name
        withName = function(self, name)
            local other = copy(self)
            other.name = name
            return other
        end,

        --- Creates a copy of this UnitBuilder
        -- @param self This UnitBuilder
        -- @return A copy of this UnitBuilder
        copy = function(self)
            return copy(self)
        end
    }
end

UnitBuilder.Ground = {}
UnitBuilder.Ground.BTR80 = function() return UnitBuilder.New("BTR-80") end
UnitBuilder.Ground.MORTAR2B11 = function() return UnitBuilder.New("2B11 mortar") end
UnitBuilder.Ground.SAU_GVOZDIKA = function() return UnitBuilder.New("SAU Gvozdika") end
UnitBuilder.Ground.SAU_MSTA = function() return UnitBuilder.New("SAU Msta") end
UnitBuilder.Ground.SAU_AKATSIA = function() return UnitBuilder.New("SAU Akatsia") end
UnitBuilder.Ground.SAU_2_C9 = function() return UnitBuilder.New("SAU 2-C9") end
UnitBuilder.Ground.M_109 = function() return UnitBuilder.New("M-109") end
UnitBuilder.Ground.SPGH_DANA = function() return UnitBuilder.New("SpGH_Dana") end
UnitBuilder.Ground.AAV7 = function() return UnitBuilder.New("AAV7") end
UnitBuilder.Ground.BMD_1 = function() return UnitBuilder.New("BMD-1") end
UnitBuilder.Ground.BMP_1 = function() return UnitBuilder.New("BMP-1") end
UnitBuilder.Ground.BMP_2 = function() return UnitBuilder.New("BMP-2") end
UnitBuilder.Ground.BMP_3 = function() return UnitBuilder.New("BMP-3") end
UnitBuilder.Ground.BRDM_2 = function() return UnitBuilder.New("BRDM-2") end
UnitBuilder.Ground.BTR_D = function() return UnitBuilder.New("BTR_D") end
UnitBuilder.Ground.BUNKER = function() return UnitBuilder.New("Bunker") end
UnitBuilder.Ground.COBRA = function() return UnitBuilder.New("Cobra") end
UnitBuilder.Ground.GRAD_FDDM = function() return UnitBuilder.New("Grad_FDDM") end
UnitBuilder.Ground.LAV_25 = function() return UnitBuilder.New("LAV-25") end
UnitBuilder.Ground.M1043_HMMWV = function() return UnitBuilder.New("M1043 HMMWV Armament") end
UnitBuilder.Ground.M1045_HMMWV_TOW = function() return UnitBuilder.New("M1045 HMMWV TOW") end
UnitBuilder.Ground.M1126_STRYKER_ICV = function() return UnitBuilder.New("M1126 Stryker ICV") end
UnitBuilder.Ground.M_113 = function() return UnitBuilder.New("M-113") end
UnitBuilder.Ground.M1134_STRYKER_ATGM = function() return UnitBuilder.New("M1134 Stryker ATGM") end
UnitBuilder.Ground.M2_BRADLEY = function() return UnitBuilder.New("M-2 Bradley") end
UnitBuilder.Ground.MARDER = function() return UnitBuilder.New("Marder") end
UnitBuilder.Ground.MCV_80 = function() return UnitBuilder.New("MCV-80") end
UnitBuilder.Ground.MLRS_FDDM = function() return UnitBuilder.New("MLRS FDDM") end
UnitBuilder.Ground.MTLB = function() return UnitBuilder.New("MTLB") end
UnitBuilder.Ground.PARATROOPER_RPG16 = function() return UnitBuilder.New("Paratrooper RPG-16") end
UnitBuilder.Ground.PARATROOPER_AKS74 = function() return UnitBuilder.New("Paratrooper AKS-74") end
UnitBuilder.Ground.SANDBOX = function() return UnitBuilder.New("Sandbox") end
UnitBuilder.Ground.SOLDIER_AK = function() return UnitBuilder.New("Soldier AK") end
UnitBuilder.Ground.INFANTRY_AK_INS = function() return UnitBuilder.New("Infantry AK Ins") end
UnitBuilder.Ground.INFANTRY_AK = function() return UnitBuilder.New("Infantry AK") end
UnitBuilder.Ground.SOLDIER_M249 = function() return UnitBuilder.New("Soldier M249") end
UnitBuilder.Ground.SOLDIER_M4 = function() return UnitBuilder.New("Soldier M4") end
UnitBuilder.Ground.SOLDIER_M4_GRG = function() return UnitBuilder.New("Soldier M4 GRG") end
UnitBuilder.Ground.SOLDIER_RPG = function() return UnitBuilder.New("Soldier RPG") end
UnitBuilder.Ground.TPZ = function() return UnitBuilder.New("TPZ") end
UnitBuilder.Ground.GRAD_URAL = function() return UnitBuilder.New("Grad-URAL") end
UnitBuilder.Ground.URAGAN_BM27 = function() return UnitBuilder.New("Uragan_BM-27") end
UnitBuilder.Ground.SMERCH = function() return UnitBuilder.New("Smerch") end
UnitBuilder.Ground.MLRS = function() return UnitBuilder.New("MLRS") end
UnitBuilder.Ground.TUNGUSKA_2S6 = function() return UnitBuilder.New("2S6 Tunguska") end
UnitBuilder.Ground.KUB_2P25_LN = function() return UnitBuilder.New("Kub 2P25 ln") end
UnitBuilder.Ground.S125_LN_5P73 = function() return UnitBuilder.New("5p73 s-125 ln") end
UnitBuilder.Ground.S300PS_5P85C_LN = function() return UnitBuilder.New("S-300PS 5P85C ln") end
UnitBuilder.Ground.S300PS_5P85D_LN = function() return UnitBuilder.New("S-300PS 5P85D ln") end
UnitBuilder.Ground.SA11_BUK_LN = function() return UnitBuilder.New("SA-11 Buk LN 9A310M1") end
UnitBuilder.Ground.OSA_9A33_LN = function() return UnitBuilder.New("Osa 9A33 ln") end
UnitBuilder.Ground.TOR_9A331 = function() return UnitBuilder.New("Tor 9A331") end
UnitBuilder.Ground.STRELA_10M3 = function() return UnitBuilder.New("Strela-10M3") end
UnitBuilder.Ground.STRELA1_9P31 = function() return UnitBuilder.New("Strela-1 9P31") end
UnitBuilder.Ground.SA11_BUK_CC = function() return UnitBuilder.New("SA-11 Buk CC 9S470M1") end
UnitBuilder.Ground.SA8_OSA_LD = function() return UnitBuilder.New("SA-8 Osa LD 9T217") end
UnitBuilder.Ground.PATRIOT_AMG = function() return UnitBuilder.New("Patriot AMG") end
UnitBuilder.Ground.PATRIOT_ECS = function() return UnitBuilder.New("Patriot ECS") end
UnitBuilder.Ground.GEPARD = function() return UnitBuilder.New("Gepard") end
UnitBuilder.Ground.HAWK_PCP = function() return UnitBuilder.New("Hawk pcp") end
UnitBuilder.Ground.SA18_IGLA_MANPAD = function() return UnitBuilder.New("SA-18 Igla manpad") end
UnitBuilder.Ground.SA18_IGLA_COMM = function() return UnitBuilder.New("SA18 Igla comm") end
UnitBuilder.Ground.IGLA_MANPAD_INS = function() return UnitBuilder.New("Igla manpad INS") end
UnitBuilder.Ground.SA18_IGLAS_MANPAD = function() return UnitBuilder.New("SA-18 Igla-S manpad") end
UnitBuilder.Ground.SA18_IGLAS_COMM = function() return UnitBuilder.New("SA-18 Igla-S comm") end
UnitBuilder.Ground.VULCAN = function() return UnitBuilder.New("Vulcan") end
UnitBuilder.Ground.HAWK_LN = function() return UnitBuilder.New("Hawk ln") end
UnitBuilder.Ground.M48_CHAPARRAL = function() return UnitBuilder.New("M48 Chaparral") end
UnitBuilder.Ground.M6_LINEBACKER = function() return UnitBuilder.New("M6 Linebacker") end
UnitBuilder.Ground.PATRIOT_LN = function() return UnitBuilder.New("Patriot ln") end
UnitBuilder.Ground.M1097_AVENGER = function() return UnitBuilder.New("M1097 Avenger") end
UnitBuilder.Ground.PATRIOT_EPP = function() return UnitBuilder.New("Patriot EPP") end
UnitBuilder.Ground.PATRIOT_CP = function() return UnitBuilder.New("Patriot cp") end
UnitBuilder.Ground.EWR_1L13 = function() return UnitBuilder.New("1L13 EWR") end
UnitBuilder.Ground.KUB_1S91_STR = function() return UnitBuilder.New("Kub 1S91 str") end
UnitBuilder.Ground.S300PS_40B6M_TR = function() return UnitBuilder.New("S-300PS 40B6M tr") end
UnitBuilder.Ground.S300PS_40B6MD_SR = function() return UnitBuilder.New("S-300PS 40B6MD sr") end
UnitBuilder.Ground.EWR_55G6 = function() return UnitBuilder.New("55G6 EWR") end
UnitBuilder.Ground.S300PS_64H6E_SR = function() return UnitBuilder.New("S-300PS 64H6E sr") end
UnitBuilder.Ground.SA11_BUK_SR_9S18M1 = function() return UnitBuilder.New("SA-11 Buk SR 9S18M1") end
UnitBuilder.Ground.DOG_EAR_RADAR = function() return UnitBuilder.New("Dog Ear radar") end
UnitBuilder.Ground.HAWK_TR = function() return UnitBuilder.New("Hawk tr") end
UnitBuilder.Ground.HAWK_SR = function() return UnitBuilder.New("Hawk sr") end
UnitBuilder.Ground.PATRIOT_STR = function() return UnitBuilder.New("Patriot str") end
UnitBuilder.Ground.HAWK_CWAR = function() return UnitBuilder.New("Hawk cwar") end
UnitBuilder.Ground.P19_S125_SR = function() return UnitBuilder.New("p-19 s-125 sr") end
UnitBuilder.Ground.ROLAND_RADAR = function() return UnitBuilder.New("Roland Radar") end
UnitBuilder.Ground.SNR_S125_TR = function() return UnitBuilder.New("snr s-125 tr") end
UnitBuilder.Ground.ROLAND_ADS = function() return UnitBuilder.New("Roland ADS") end
UnitBuilder.Ground.S300PS_54K6_CP = function() return UnitBuilder.New("S-300PS 54K6 cp") end
UnitBuilder.Ground.STINGER_COMM_DSR = function() return UnitBuilder.New("Stinger comm dsr") end
UnitBuilder.Ground.STINGER_COMM = function() return UnitBuilder.New("Stinger comm") end
UnitBuilder.Ground.SOLDIER_STINGER = function() return UnitBuilder.New("Soldier stinger") end
UnitBuilder.Ground.ZSU234_SHILKA = function() return UnitBuilder.New("ZSU-23-4 Shilka") end
UnitBuilder.Ground.ZU23_EMPLACEMENT_CLOSED = function() return UnitBuilder.New("ZU-23 Emplacement Closed") end
UnitBuilder.Ground.ZU23_EMPLACEMENT = function() return UnitBuilder.New("ZU-23 Emplacement") end
UnitBuilder.Ground.ZU23_CLOSED_INSURGENT = function() return UnitBuilder.New("ZU-23 Closed Insurgent") end
UnitBuilder.Ground.URAL375_ZU23_INSURGENT = function() return UnitBuilder.New("Ural-375 ZU-23 Insurgent") end
UnitBuilder.Ground.ZU23_INSURGENT = function() return UnitBuilder.New("ZU-23 Insurgent") end
UnitBuilder.Ground.URAL375_ZU23 = function() return UnitBuilder.New("Ural-375 ZU-23") end
UnitBuilder.Ground.HOUSE1ARM = function() return UnitBuilder.New("house1arm") end
UnitBuilder.Ground.HOUSE2ARM = function() return UnitBuilder.New("house2arm") end
UnitBuilder.Ground.OUTPOST_ROAD = function() return UnitBuilder.New("outpost_road") end
UnitBuilder.Ground.OUTPOST = function() return UnitBuilder.New("outpost") end
UnitBuilder.Ground.HOUSEA_ARM = function() return UnitBuilder.New("houseA_arm") end
UnitBuilder.Ground.TACAN_BEACON = function() return UnitBuilder.New("TACAN_beacon") end
UnitBuilder.Ground.CHALLENGER2 = function() return UnitBuilder.New("Challenger2") end
UnitBuilder.Ground.LECLERC = function() return UnitBuilder.New("Leclerc") end
UnitBuilder.Ground.LEOPARD1A3 = function() return UnitBuilder.New("Leopard1A3") end
UnitBuilder.Ground.LEOPARD2 = function() return UnitBuilder.New("Leopard-2") end
UnitBuilder.Ground.M60 = function() return UnitBuilder.New("M-60") end
UnitBuilder.Ground.M1128_STRYKER_MGS = function() return UnitBuilder.New("M1128 Stryker MGS") end
UnitBuilder.Ground.M1_ABRAMS = function() return UnitBuilder.New("M-1 Abrams") end
UnitBuilder.Ground.MERKAVA_MK4 = function() return UnitBuilder.New("Merkava_Mk4") end
UnitBuilder.Ground.T55 = function() return UnitBuilder.New("T-55") end
UnitBuilder.Ground.T72B = function() return UnitBuilder.New("T-72B") end
UnitBuilder.Ground.T80UD = function() return UnitBuilder.New("T-80UD") end
UnitBuilder.Ground.T90 = function() return UnitBuilder.New("T-90") end
UnitBuilder.Ground.URAL4320_APA5D = function() return UnitBuilder.New("Ural-4320 APA-5D") end
UnitBuilder.Ground.ATMZ5 = function() return UnitBuilder.New("ATMZ-5") end
UnitBuilder.Ground.ATZ10 = function() return UnitBuilder.New("ATZ-10") end
UnitBuilder.Ground.GAZ3307 = function() return UnitBuilder.New("GAZ-3307") end
UnitBuilder.Ground.GAZ3308 = function() return UnitBuilder.New("GAZ-3308") end
UnitBuilder.Ground.GAZ66 = function() return UnitBuilder.New("GAZ-66") end
UnitBuilder.Ground.M978_HEMTT_Tanker = function() return UnitBuilder.New("M978 HEMTT Tanker") end
UnitBuilder.Ground.HEMTT_TFFT = function() return UnitBuilder.New("HEMTT TFFT") end
UnitBuilder.Ground.IKARUS_BUS = function() return UnitBuilder.New("IKARUS Bus") end
UnitBuilder.Ground.KAMAZ_TRUCK = function() return UnitBuilder.New("KAMAZ Truck") end
UnitBuilder.Ground.KRAZ6322 = function() return UnitBuilder.New("KrAZ6322") end
UnitBuilder.Ground.LAZ_BUS = function() return UnitBuilder.New("LAZ Bus") end
UnitBuilder.Ground.HUMMER = function() return UnitBuilder.New("Hummer") end
UnitBuilder.Ground.M_818 = function() return UnitBuilder.New("M 818") end
UnitBuilder.Ground.MAZ6303 = function() return UnitBuilder.New("MAZ-6303") end
UnitBuilder.Ground.PREDATOR_GCS = function() return UnitBuilder.New("Predator GCS") end
UnitBuilder.Ground.PREDATOR_TROJANSPIRIT = function() return UnitBuilder.New("Predator TrojanSpirit") end
UnitBuilder.Ground.SUIDAE = function() return UnitBuilder.New("Suidae") end
UnitBuilder.Ground.TIGR_233036 = function() return UnitBuilder.New("Tigr_233036") end
UnitBuilder.Ground.TROLLEY_BUS = function() return UnitBuilder.New("Trolley bus") end
UnitBuilder.Ground.UAZ469 = function() return UnitBuilder.New("UAZ-469") end
UnitBuilder.Ground.URAL_ATSP6 = function() return UnitBuilder.New("Ural ATsP-6") end
UnitBuilder.Ground.URAL375_PBU = function() return UnitBuilder.New("Ural-375 PBU") end
UnitBuilder.Ground.URAL375 = function() return UnitBuilder.New("Ural-375") end
UnitBuilder.Ground.URAL432031 = function() return UnitBuilder.New("Ural-4320-31") end
UnitBuilder.Ground.URAL4320T = function() return UnitBuilder.New("Ural-4320T") end
UnitBuilder.Ground.VAZ_CAR = function() return UnitBuilder.New("VAZ Car") end
UnitBuilder.Ground.ZIL131_APA80 = function() return UnitBuilder.New("ZiL-131 APA-80") end
UnitBuilder.Ground.SKP11 = function() return UnitBuilder.New("SKP-11") end
UnitBuilder.Ground.ZIL131_KUNG = function() return UnitBuilder.New("ZIL-131 KUNG") end
UnitBuilder.Ground.ZIL4331 = function() return UnitBuilder.New("ZIL-4331") end

return UnitBuilder
