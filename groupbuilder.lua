--- Initial comment
local module_folder = lfs.writedir()..[[Scripts\groupbuilder\]]
package.path = module_folder .. "?.lua;" .. package.path

local function copy(obj, seen)
  if type(obj) ~= 'table' then return obj end
  if seen and seen[obj] then return seen[obj] end
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do res[copy(k, s)] = copy(v, s) end
  return res
end

GroupBuilder = {}
GroupBuilder.SpawnFunction = mist.dynAdd
GroupBuilder.StaticSpawnFunction = mist.dynAddStatic
GroupBuilder.SpawnedGroupNames = {}


--- Manipulate the group name so that it's unique and doesn't cause units to despawn.
-- @param groupName The attempted groupname.
-- @return A valid name. It may be new, or it may be the same.
local function validateGroupName(groupName)
    local spawned = GroupBuilder.SpawnedGroupNames[groupName]
    if spawned == nil then
        GroupBuilder.SpawnedGroupNames[groupName] = 1
        return groupName
    else
        GroupBuilder.SpawnedGroupNames[groupName] = spawned + 1
        return groupName .."-"..spawned
    end
end

--- Updates the positions of the group's units to be centered
-- around the provided position.
-- We don't modify the original units table.
-- @param units The list of units to which we apply x,y properties.
-- @param position The {x=X,y=Y} table indicating a position.
-- @return a fresh copy of the units table, where the units are centered around {{position}}
local function updatePositions(units, position)
    local applyOffset = function(unit)
        local offset
        if unit.offset then
            offset = unit.offset
        else
            offset = {
                x = math.random(-15, 15),
                y = math.random(-15, 15)
            }
        end
        unit["x"] = position.x + offset.x
        unit["y"] = position.y + offset.y
    end
    local units_clone = copy(units)
    --Handle offset later.
    for _,unit in pairs(units_clone) do
        applyOffset(unit)
    end
    return units_clone
end

--- Creates a new GroupBuilder object.
-- Check each method's doc for ways to interact with this.
-- @param name The group name.
-- @return A new GroupBuilder
GroupBuilder.New = function(name)
    return {
        spawnFunction = GroupBuilder.SpawnFunction,
        name = name,
        units = {}, -- Don't want it to be nil
        category = Group.Category.GROUND,


        --- Creates a copy of this GroupBuilder object with the group name of {{name}}.
        -- @param self This GroupBuilder.
        -- @param name The new name of the group.
        -- @return A New GroupBuilder where name is {{name}}
        withName = function(self, name)
            local other = copy(self)
            other.name = name
            return other
        end,

        --- Creates a copy of this GroupBuilder with the country set to {{country}}
        -- @param self This GroupBuilder
        -- @param country The country id (integer) provided by DCS.
        -- @return A New GroupBuilder where country is {{country}}
        withCountry = function(self, country)
            local other = copy(self)
            other.country = country
            return other
        end,

        --- Creates a copy of this GroupBuilder with the units set to {{units}}
        -- @param self This GroupBuilder
        -- @param units The list of units that we want to add to this group.
        -- @return A new GroupBuilder where the units are {{unit}}
        withUnits = function(self, units)
            local other = copy(self)
            other.units = units
            return other
        end,

        --- Creates a copy of this GroupBuilder with the position of the group set.
        -- If not set we just use the unit's positions.
        -- If set, we overwrite the units positions. This may change.
        -- @param self This GroupBuilder
        -- @param position The position in {x,y} coordinates to center the group spawn.
        atPosition = function(self, position)
            local other = copy(self)
            other.position = position
            return other
        end,


        -- Convenience because I keep using this instead of atPosition
        withPosition = function(self, position) return self:atPosition(position) end,

        --- Takes a group and applies its properties to this builder.
        ---@param self any
        ---@param groupData any
        withGroupData = function(self, groupData)
            local other = copy(self)
            for k,v in pairs(groupData) do
                other[k] = v
            end
            return other
        end,

        --- Sends a copy of this group builder into the spawning function.
        -- Defaults to mist.dynAdd, but can be whatever.
        -- @param self This GroupBuilder
        -- @return The response from the spawnFunction
        spawn = function(self)
            local other = copy(self)
            if self.position then
                other.units = updatePositions(self.units, self.position)
                -- Groups with ground units don't need this.
                -- I do it here to shoehorn static structure support in here.
                other.x = other.position.x
                other.y = other.position.y
            end
            other.name = validateGroupName(self.name)
            return self.spawnFunction(other)
        end
    }
end


--- Get a shoehorned version of groupbuilder that tries to handle a static.
--- Not guaranteed to work in all cases...
GroupBuilder.Static = function(staticType)
    local builder = GroupBuilder.New("Static-"..staticType)
    builder.spawnFunction = GroupBuilder.StaticSpawnFunction
    builder.category = 'Fortifications' -- probably covers most. Still overridable.
    builder.type = staticType
    return builder
end

-- local btr80 = UnitBuilder.New("BTR-80")
-- local myGroup = GroupBuilder.New("TestGroup"):withCountry(0)
-- local pt = { y = 645907, x = -281670 }
-- myGroup = myGroup:withUnits({btr80, copy(btr80):withOffset({x=500,y=500}), copy(btr80)}):atPosition(pt)
-- myGroup:spawn()

return GroupBuilder