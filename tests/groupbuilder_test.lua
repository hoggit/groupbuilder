--Groupbuilder
package.path = "../".. "?.lua;" .. package.path

local test = require "u-test"
local groupbuilder = require "groupbuilder"
local t = test.groupbuilder

t.New_should_create_a_group_with_a_name = function()
    local g = groupbuilder.New("testGroup")
    test.equal(g.name, "testGroup")
end

t.New_should_create_a_group_with_defaults = function()
    local g = groupbuilder.New("default_group")
    test.equal(g.name, "default_group")
    test.equal(g.category, Group.Category.GROUND)
end

t.withName_should_change_names= function()
    local g = groupbuilder.New("myGroup")
    test.equal(g:withName("foo").name, "foo")
end

t.withName_should_not_modify_original = function()
    local g = groupbuilder.New("myOtherGroup")
    g:withName("foo")
    test.equal(g.name, "myOtherGroup")
end

t.withCountry_should_update_the_country = function()
    local g = groupbuilder.New("myGroup")
    local cg = g:withCountry(5)
    test.equal(cg.country, 5)
end

t.withCountry_should_not_modify_original = function()
    local g = groupbuilder.New("grp")
    g:withCountry(2)
    test.equal(g.country, nil)
end

t.withUnits_should_update_units = function()
    local g = groupbuilder.New("units")
    local units = {"foo","bar"}
    local ug = g:withUnits(units)
    test.equal(ug.units, units)
end

t.withUnits_should_not_modify_original = function()
    local g = groupbuilder.New("unitscopy")
    g:withUnits({"foo","bar"})
    test.is_not_nil(g.units)
    test.is_nil(next(g.units)) -- list is empty
end

t.atPosition_should_update_position = function()
    local position = {x = 50, y = -105}
    local g = groupbuilder.New("pos"):atPosition(position)
    test.equal(g.position.x, 50)
    test.equal(g.position.y, -105)
end

-- t.s_should_call_the_spawnFunction = function()
--     local unitbuilder = require "../unitbuilder"
--     local u = unitbuilder.New("BTR-80")
--     local g = groupbuilder.New("myGroup"):withUnits({u})
-- end