package.path = "../".. "?.lua;" .. package.path

local test = require 'u-test'
local unitbuilder = require "unitbuilder"
local t = test.unitbuilder

t.should_create_a_BTR_80_unit_with_defaults = function ()
    local btr80 = unitbuilder.New("BTR-80")
    test.equal(btr80.type, "BTR-80")
    test.equal(btr80.skill, "Average")
    test.equal(btr80.heading, 0)
    test.equal(btr80.playerCanDrive, false)
end

t.should_let_me_configure_the_type_in_a_copy = function ()
    local u = unitbuilder.New("Unit")
    local u2 = u:withType("BTR-80")
    test.equal(u.type, "Unit")
    test.equal(u2.type, "BTR-80")
end

t.should_let_me_configure_the_skill_in_a_copy = function()
    local u = unitbuilder.New("Unit")
    local u2 = u:withSkill("Excellent")
    test.equal(u.skill, "Average")
    test.equal(u2.skill, "Excellent")
end

t.should_let_me_configure_the_heading_in_a_copy = function()
    local u = unitbuilder.New("Unit")
    local u2 = u:withHeading(-128)
    test.equal(u.heading, 0)
    test.equal(u2.heading, -128)
end

t.should_let_me_set_position_in_a_copy = function()
    local u = unitbuilder.New("Unit")
    local u2 = u:withPosition({x=4,y=9})
    test.equal(u.position, nil)
    test.equal(u2.position.x, 4)
    test.equal(u2.position.y, 9)
end

t.should_set_unit_x_and_y_when_setting_position = function()
    local u = unitbuilder.New("Unit"):withPosition({x=123,y=-132})
    test.equal(u.x, 123)
    test.equal(u.y, -132)
end