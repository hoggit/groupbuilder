require "dcs_fixtures"
local function test(name)
    print("==========" .. name .. "==========")
    require(name)
end

local tests = require "u-test"

test("groupbuilder_test")
test("unitbuilder_test")
test("grouploader_test")

tests.summary()