
--Grouploader
package.path = "../".. "?.lua;" .. package.path

local test = require "u-test"
local grouploader = require "grouploader"
local t = test.grouploader

t.LoadFile_should_read_a_template_and_return_the_group = function()
    local template = grouploader.LoadFile('../templates/FoobarTemplate.stm')
    test.is_not_nil(template["Ground-1"])
    test.equal(template["Ground-1"]["name"],"Ground-1")
end