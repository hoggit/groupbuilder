local module_folder = lfs.writedir()..[[Scripts\groupbuilder\]]
package.path = module_folder .. "?.lua;" .. package.path
local GroupBuilder = require('groupbuilder')

local function loadTemplateFile(templateFile)
    local env = setmetatable({}, {__index=_G})
    assert(pcall(setfenv(assert(loadfile(templateFile)), env)))
    setmetatable(env, nil)
    return env
end

--- Kind of yuck. Not really sure of a better way though..
local function parseGroups(template)
    if template == nil then return end
    local groups, statics, planes = {}, {}, {}
    for _,coalition in pairs(template["coalition"]) do
        for _, country in pairs(coalition["country"]) do
            if country["vehicle"] and country["vehicle"]["group"] then
                for _, group in pairs(country["vehicle"]["group"]) do
                    groups[group["name"]] = group
                end
            end
            if country["static"] and country["static"]["group"] then
                for _, s in pairs(country["static"]["group"]) do
                    statics[s["name"]] = s
                end
            end
            if country["plane"] and country["plane"]["group"] then
                for _, plane in pairs(country["plane"]["group"]) do
                    planes[plane["name"]] = plane
                end
            end
        end
    end
    return groups, statics, planes
end

GroupLoader = {}


--- Loads groups from the provided template fileName
---@param fileName string The path to the template to load
---@return table A map of constructed GroupBuilders keyed by Group name.
GroupLoader.LoadFile = function(fileName)
    local template = loadTemplateFile(fileName)

    local groups, statics, planes = parseGroups(template.staticTemplate)
    local builders = {}
    for name, group in pairs(statics) do
        local builder = GroupBuilder.Static(name)
        for k,v in pairs(group) do builder[k] = v end
        builders[name] = builder
    end
    for name, group in pairs(groups) do
        local builder = GroupBuilder.New(name)
        for k,v in pairs(group) do builder[k] = v end
        builders[name] = builder
    end
    for name, group in pairs(planes) do
        local builder = GroupBuilder.New(name)
        for k,v in pairs(group) do builder[k] = v end
        builders[name] = builder
    end
    return builders
end

return GroupLoader